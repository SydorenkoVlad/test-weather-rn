export const DATA_AVAILABLE = 'DATA_AVAILABLE';

//Import the sample data
import Data from '../instructions.json';
 
export function getData(){
    return (dispatch) => {
 
        //Make API Call
        //For this example, I will be using the sample data in the json file
        //delay the retrieval [Sample reasons only]
        setTimeout(() => {

            const data = Data.instructions;
            dispatch({type: DATA_AVAILABLE, data:data});
        }, 2000);
 
    };
}

export function getCityWeather() {
    return (dispatch) => {
        setTimeout(() => {
            let theUrl = "http://api.openweathermap.org/data/2.5/weather?q=Kiev&APPID=465db2d5bb1bb8b40393a6ec0691a363";

            function parseData(response) {
                dispatch({type: DATA_AVAILABLE, data: JSON.parse(response)});   
            }
            
            var xmlHttp = new XMLHttpRequest();

            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                  //console.log("SUCESS REQUSET from actions");
                  parseData(xmlHttp.responseText);
                }   
            }

            xmlHttp.open("GET", theUrl, true); // true for asynchronous 
            xmlHttp.send(null);

            

        }, 2000);
    };
}