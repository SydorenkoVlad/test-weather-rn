import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Hideo } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions'; //Import your actions

import SplashScreen from './SplashScreen';

class Home extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: {},
        textValue: '',
        isVisible: true
      }
    }
  
    componentDidMount() {
        let self = this;

        setTimeout(() => {
          self.Hide_Splash_Screen();
        }, 2000);

        this.props.getCityWeather();
    }

    Hide_Splash_Screen = () => {
      this.setState({ 
        isVisible : false 
      });
    }

    render() {
      if(this.state.isVisible) {
        return (
          <View style={styles.container}>
            <SplashScreen />
          </View>
        )
      }
      else {
        return (
          <View style={styles.container}>
            <Hideo
              style={styles.hideoContainer}
              iconClass={FontAwesomeIcon}
              iconName={'globe'}
              iconColor={'#00FF7F'}
              iconBackgroundColor={'#6A5ACD'}
              inputStyle={{ color: '#464949' }}
              onChangeText={(text) => { this.setState({textValue: text}) }}
              />
              <Text>Weather for {this.props.data.name}</Text>
          </View>
        );
      } 
    }
  }

  // The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data
    }
  }
  
  // Doing this merges our actions into the component’s props,
  // while wrapping them in dispatch() so that they immediately dispatch an Action.
  // Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
  }
  
  //Connect everything
  export default connect(mapStateToProps, mapDispatchToProps)(Home);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ccffff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
      margin: 15,
      paddingLeft: 15,
      paddingRight: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
    },
    hideoContainer: {
      margin: 30
    }
  });