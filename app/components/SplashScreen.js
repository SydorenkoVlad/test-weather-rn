import React from 'react';
import { StyleSheet, View, Image, Animated, Easing } from 'react-native';

export default class SplashScreen extends React.Component {
    constructor () {
        super()
        this.spinValue = new Animated.Value(0)
    }

    componentDidMount() {
        this.spin()
    }

    spin() {
        this.spinValue.setValue(0)
        Animated.timing(
          this.spinValue,
          {
            toValue: 1,
            duration: 1500,
            easing: Easing.linear
          }
        ).start(() => this.spin())
    }

    render() {
        
        const spin = this.spinValue.interpolate({
           inputRange: [0, 1],
           outputRange: ['0deg', '360deg']
        });

        return (
            <View style={styles.SplashScreen_RootView}>
                <View style={styles.SplashScreen_ChildView}>
 
                    {/* Put all your components Image and Text here inside Child view which you want to show in Splash Screen. */}
                    <Animated.Image
                      style={{
                        width: '70%',
                        height: '70%',
                        resizeMode: 'contain',
                        transform: [{rotate: spin}] }}
                        source={ require('./../assets/images/sunLogo.png')}
                    />
                    {/* <Image source={require('./../assets/images/sunLogo.png')}
                    style={{width:'70%', height:'70%', resizeMode: 'contain'}} /> */}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    SplashScreen_RootView: {
        justifyContent: 'center',
        flex:1,
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    SplashScreen_ChildView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ccffff',
        flex:1,
        margin: 20,
    }
})